# BuildStream Demo

This is a demo showing how to build
[Apache Arrow](https://github.com/apache/arrow/) in a sandbox using BuildStream.

This demo was created for a talk at the [London Build Meetup 2019](https://www.eventbrite.com/e/london-build-meetup-tickets-70824519043).

Slides for the talk are [here](TODO: ADD LINK)


## Why Apache Arrow?

Apache Arrow is a large project with many dependencies.
In order to make developer life easier, their build system will download build
dependencies as needed, or use the system one if they are present.

This makes is hard to know against which versions of which software Apache
Arrow was built or to setup a working development setup.

Using BuildStream allows us to specify exactly what is used to build Apache
Arrow and makes it simple to build: `bst build arrow.bst` is sufficient.


## How to use

For convenience, a set of docker containers are put to disposition as aprt of
a docker-compose setup.

- artifact_cache: this is for storing artifacts built by BuildStream
- source_cache: this is for storing sources downloaded by BuildStream
- buildstream: this is an image to run a preconfigured BuildStream with all
dependencies setup.

The two cache images allow you to mimic an organisational usage where sources
and artifacts may be shared between developers.

In order to start the two caches:

```bash
docker-compose up -d artifact_cache source_cache
```

In order to start playing with BuildStream:

```bash
docker-compose run buildstream
```

Will give you a shell. You can then run:

```bash
bst --help
bst build arrow.bst
```

In order to get information or start building Apache Arrow.
