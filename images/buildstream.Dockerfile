##############
# Base image #
##############
FROM ubuntu:rolling as base

ENV PYTHON=python3.7

RUN \
    # Disable recommended and suggested packages
    echo 'APT::Install-Recommends "0";' > /etc/apt/apt.conf.d/20-no-recommends && \
    echo 'APT::Install-Suggests "0";' > /etc/apt/apt.conf.d/20-no-suggests && \
    apt-get update && \
    apt-get install --assume-yes \
        gosu \
        ${PYTHON} \
        python3-pip \
    && \
    rm -rf /var/lib/apt/lists/*


###########
# Builder #
###########
FROM base as builder

# Install system dependencies
RUN \
    apt-get update && \
    apt-get install --assume-yes \
        cmake \
        gcc \
        g++ \
        git \
        libgrpc++-dev \
        libprotobuf-dev \
        libssl-dev \
        make \
        pkg-config \
        protobuf-compiler \
        protobuf-compiler-grpc \
        python3-dev \
        python3-setuptools \
        python3-wheel \
        uuid-dev \
        # Ostree dependencies
        libcairo2-dev \
        libgirepository1.0-dev

# Build and install buildbox-common
ARG BUILDBOX_COMMON_VERSION

RUN \
    git clone https://gitlab.com/BuildGrid/buildbox/buildbox-common.git /build/buildbox-common && \
    cd /build/buildbox-common && \
    git checkout ${BUILDBOX_COMMON_VERSION} && \
    cmake \
        -DBUILD_TESTING=OFF \
        -DCMAKE_INSTALL_PREFIX=/usr \
        . -Bbuild \
    && \
    make -C build -j $(nproc) && \
    make -C build install

# Build buildbox-casd
ARG BUILDBOX_CASD_VERSION

RUN \
    git clone https://gitlab.com/BuildGrid/buildbox/buildbox-casd.git /build/buildbox-casd && \
    cd /build/buildbox-casd && \
    git checkout ${BUILDBOX_CASD_VERSION} && \
    cmake \
        -DBUILD_TESTING=OFF \
        . -Bbuild \
    && \
    make -C build -j $(nproc) && \
    make -C build DESTDIR=/artifacts/ install

# Build python wheels
ARG BUILDSTREAM_VERSION
ARG BST_PLUGINS_EXPERIMENTAL_VERSION

RUN \
    ${PYTHON} -m pip wheel --wheel-dir /artifacts/wheels \
        # Needed by Ostree
        PyGobject \
        git+https://gitlab.com/buildstream/buildstream@${BUILDSTREAM_VERSION} \
        git+https://gitlab.com/BuildStream/bst-plugins-experimental@${BST_PLUGINS_EXPERIMENTAL_VERSION}


##########################
# BuildStream thin image #
##########################
FROM base as buildstream_thin

COPY --from=builder /artifacts /artifacts

RUN \
    apt-get update && \
    apt-get install --assume-yes \
        # Buildbox-casd
        libgrpc++ \
        libprotobuf17 \
    && \
    # Install wheels
    ${PYTHON} -m pip install /artifacts/wheels/* && \
    # Install buildbox-casd
    cp /artifacts/usr/local/bin/buildbox-casd /usr/local/bin/buildbox-casd && \
    # Cleanup
    rm -r /artifacts && \
    rm -rf /var/lib/apt/lists/*


############################
# BuildStream client image #
############################
FROM buildstream_thin as buildstream

ADD buildstream.entrypoint.sh /usr/local/bin/entrypoint
ADD buildstream.conf /etc/buildstream.conf

RUN \
    chmod +x /usr/local/bin/entrypoint && \
    apt-get update && \
    apt-get install --assume-yes \
        # Sandboxing
        bubblewrap \
        fuse \
        # Plugins
        gir1.2-ostree-1.0 \
        git \
        lzip \
        quilt \
        # Useful utilities
        bash-completion \
        less \
        vim \
        sudo \
    && \
    # Install needed python plugins
    ${PYTHON} -m pip install \
        pytoml \
    && \
    # Allow sudoing as any user
    echo "buildstream ALL=(root) NOPASSWD: ALL" > /etc/sudoers.d/buildstream && \
    useradd -m buildstream && \
    mkdir -p /home/buildstream/.config && \
    cp /etc/buildstream.conf /home/buildstream/.config/buildstream.conf && \
    chown -R buildstream:buildstream /home/buildstream && \
    # Cleanup
    rm -rf /var/apt/lists/*

ENTRYPOINT ["/usr/local/bin/entrypoint"]
CMD ["/bin/bash"]
WORKDIR /home/buildstream/project/


##############################
# BuildStream artifactserver #
##############################
FROM buildstream_thin as artifactserver

ADD artifactserver.entrypoint.sh /usr/local/bin/entrypoint

RUN \
    chmod +x /usr/local/bin/entrypoint && \
    useradd -m buildstream

ENTRYPOINT ["/usr/local/bin/entrypoint"]


#   Copyright (C) 2019 Bloomberg Finance L.P.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
